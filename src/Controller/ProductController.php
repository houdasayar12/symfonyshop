<?php

declare(strict_types=1);

namespace App\Controller;

use App\Component\Product\Model\Product;
use App\Component\Product\Repository\ProductRepository;
use App\Form\AddItemType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    public $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/products", name="products")
     */
    public function index(): Response
    {
        $products = $this->entityManager->getRepository(Product::class)->findAll();

        return $this->render('product/index.html.twig', [
            'products' => $products,
        ]);
    }

    /**
     * @Route("/products/{slug}", name="product")
     */
    public function show(Product $product)
    {
        $form = $this->createForm(AddItemType::class);
        $products = $this->entityManager->getRepository(Product::class)->findByIsBest(1);

        if (!$product) {
            return $this->redirectToRoute('products');
        }

        return $this->render('product/show.html.twig', [
            'product' => $product,
            'products' => $products,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/search", methods="GET", name="product_search")
     */
    public function search(Request $request, ProductRepository $products): Response
    {
        $query = $request->query->get('q', '');
        $limit = $request->query->get('l', 10);

        if (!$request->isXmlHttpRequest()) {
            return $this->render('product/_search_product.html.twig', ['query' => $query]);
        }

        $foundProducts = $products->findBySearchQuery($query, $limit);

        $results = [];
        foreach ($foundProducts as $product) {
            $results[] = [
                'name' => htmlspecialchars($product->getName(), \ENT_COMPAT | \ENT_HTML5),
                'subtitle' => htmlspecialchars($product->getSubtitle(), \ENT_COMPAT | \ENT_HTML5),
                'price' => htmlspecialchars($product->getPrice(), \ENT_COMPAT | \ENT_HTML5),
                'illustration' => htmlspecialchars($product->getIllustration(), \ENT_COMPAT | \ENT_HTML5),
                'slug' => $this->generateUrl('product', ['slug' => $product->getSlug()]),
            ];
        }

        return $this->json($results);
    }
}
