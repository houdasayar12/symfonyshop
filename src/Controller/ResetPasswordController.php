<?php

declare(strict_types=1);

namespace App\Controller;

use App\Component\User\Model\ResetPassword;
use App\Component\User\Model\User;
use App\Form\ResetPasswordType;
use App\Form\ResetType;
use App\Utils\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ResetPasswordController extends AbstractController
{
    /** @var MailerService */
    private $mailerService;

    /** @var EmtityMangerInterface */
    private $entityManager;

    /** @var TranslatorInterface */
    private $translator;

    public function __construct(
        EntityManagerInterface $entityManager,
        MailerService $mailerService,
        TranslatorInterface $translator
    ) {
        $this->entityManager = $entityManager;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
    }

    /**
     * @Route("/mot-de-passe-oublie", name="reset_password")
     */
    public function reset(Request $request): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }

        $form = $this->createForm(ResetType::class);
        $form->handleRequest($request);
        if ($request->get('email')) {
            $user = $this->entityManager->getRepository(User::class)->findOneByEmail($request->get('email'));

            if ($user) {
                // 1 : Enregistrer en base la demande de reset_password avec user, token, createdAt.
                $resetPassword = new ResetPassword();
                $resetPassword->setUser($user);
                $resetPassword->setToken(uniqid());
                $resetPassword->setCreatedAt(new \DateTime());
                $this->entityManager->persist($resetPassword);
                $this->entityManager->flush();

                // 2 : Envoyer un email à l'utilisateur avec un lien lui permettant de mettre à jour son mot de passe.
                $url = $this->generateUrl(
                    'update_password',
                    [
                        'token' => $resetPassword->getToken(),
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );

                $mail = $this->mailerService->create(
                    'emails/mail.html.twig',
                    $user->getEmail(),
                    [
                        'mail' => $user->getEmail(),
                        'url' => $url,
                        'firstname' => $user->getFirstname(),
                    ]
                );

                $this->mailerService->send($mail);

                $this->translator->trans('app.user.rest.password.message.info');
                $this->addFlash(
                    'info',
                    'Vous allez recevoir dans quelques secondes un mail'.
                    'avec la procédure pour réinitialiser votre mot de passe.'
                );
            } else {
                $this->translator->trans('app.user.rest.password.message.info');
                $this->addFlash('info', 'Cette adresse email est inconnue.');
            }

            return new RedirectResponse($this->generateUrl('reset_password'));
        }

        return $this->render('reset_password/index.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/modifier-mon-mot-de-passe/{token}", name="update_password")
     */
    public function update(Request $request, $token, UserPasswordEncoderInterface $encoder)
    {
        $resetPassword = $this->entityManager->getRepository(ResetPassword::class)->findOneByToken($token);

        if (!$resetPassword) {
            return $this->redirectToRoute('reset_password');
        }

        if (null === $resetPassword->getToken() || $token !== $resetPassword->getToken()) {
            throw new AccessDeniedHttpException();
        }

        // Vérifier si le createdAt = now - 3h
        $now = new \DateTime();
        if ($now > $resetPassword->getCreatedAt()->modify('+ 3 hour')) {
            $this->translator->trans('app.user.update.password.message.info');
            $this->addFlash('info', 'Votre demande de mot de passe a expiré. Merci de la renouveller.');

            return $this->redirectToRoute('reset_password');
        }

        // Rendre une vue avec mot de passe et confirmez votre mot de passe.
        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newPwd = $form->get('new_password')->getData();

            // Encodage des mots de passe
            $password = $encoder->encodePassword($resetPassword->getUser(), $newPwd);
            $resetPassword->getUser()->setPassword($password);
            $resetPassword->setToken(null);

            // Flush en base de donnée.
            $this->entityManager->flush();

            // Redirection de l'utilisateur vers la page de connexion.
            $this->translator->trans('app.user.update.password.message.success');
            $this->addFlash('success', 'Votre mot de passe a bien été mis à jour.');

            return $this->redirectToRoute('app_login');
        }

        return $this->render('reset_password/update.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
