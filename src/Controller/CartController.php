<?php

declare(strict_types=1);

namespace App\Controller;

use App\Component\Order\Factory\OrderFactory;
use App\Component\Order\Model\OrderItem;
use App\Component\Product\Model\Product;
use App\Form\AddItemType;
use App\Form\ClearCartType;
use App\Form\RemoveItemType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class CartController extends AbstractController
{
    /** @var OrderFactory */
    private $orderFactory;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var TranslatorInterface */
    private $translator;

    public function __construct(
        OrderFactory $orderFactory,
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator
    ) {
        $this->orderFactory = $orderFactory;
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    /**
     * @Route("/cart", name="cart")
     */
    public function index(OrderFactory $order): Response
    {
        $clearForm = $this->createForm(ClearCartType::class, $order->getCurrent());

        return $this->render('cart/index.html.twig', [
            'order' => $order,
            'clearForm' => $clearForm->createView(),
            'itemsInCart' => $order->getCurrent()->getItemsTotal(),
        ]);
    }

    /**
     * @Route("/cart/addItem/{id}",name="add_to_cart" )
     */
    public function addItemToCart(Request $request, Product $product): Response
    {
        $form = $this->createForm(AddItemType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $quantity = $request->request->get('add_item')['itemQuantity'];

            $this->orderFactory->addItem($product, (int) $quantity);
            $this->translator->trans('app.cart.addItem.message.success');
            $this->addFlash('success', 'Le produit a été ajouté au panier');
        }

        return $this->redirectToRoute('cart');
    }

    public function removeItemForm(OrderItem $item): Response
    {
        $form = $this->createForm(RemoveItemType::class, $item);

        return $this->render('cart/_removeItem_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/cart/removeItem/{id}", name="cart.removeItem", methods={"POST"})
     */
    public function removeItem(Request $request, OrderItem $item): Response
    {
        $form = $this->createForm(RemoveItemType::class, $item);
        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $this->orderFactory->removeItem($item);
            $this->translator->trans('app.cart.removeItem.message.success');
            $this->addFlash('success', 'Le produit a été supprimé du panier');
        }

        return $this->redirectToRoute('cart');
    }

    /**
     * @Route("/cart/clear", name="cart.clear", methods={"POST"})
     */
    public function clear(Request $request)
    {
        $form = $this->createForm(ClearCartType::class, $this->orderFactory->getCurrent());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->orderFactory->clear();
            $this->translator->trans('app.cart.clear.message.success');
            $this->addFlash('success', 'Votre panier est vide');
        }

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/cart/setItemQuantity/{id}", name="cart.setItemQuantity", methods={"POST"})
     */
    public function setQuantity(): Response
    {
        return new Response('ok');
    }
}
