<?php

declare(strict_types=1);

namespace App\Controller;

use App\Component\Order\Factory\OrderFactory;
use App\Component\Order\Model\Order;
use App\Component\Order\Repository\OrderRepository;
use App\Form\OrderType;
use App\Message\OrderConfirmationEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @Route("/commande", name="order")
     */
    public function index(OrderFactory $order): Response
    {
        if (!$this->getUser()->getAddresses()->getValues()) {
            return $this->redirectToRoute('account_address_add');
        }
        $form = $this->createForm(OrderType::class, null, [
            'user' => $this->getUser(),
        ]);

        return $this->render('order/index.html.twig', [
            'form' => $form->createView(),
            'order' => $order,
            'itemsPriceTotal' => $order->getCurrent()->getItemsPriceTotal(),
            'itemsTotal' => $order->getCurrent()->getItemsTotal(),
            'priceTotal' => $order->getCurrent()->getPriceTotal(),
        ]);
    }

    /**
     * @Route("/commande/payer", name="order_payer", methods={"POST"})
     */
    public function add(Request $request): Response
    {
        $form = $this->createForm(OrderType::class, null, [
            'user' => $this->getUser(),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $bus->dispatch(new OrderConfirmationEmail($order->getCurrent()->getId()));
        }

        return new Response();
    }
}
