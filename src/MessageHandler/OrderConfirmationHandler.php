<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Component\Order\Repository\OrderRepository;
use App\Message\OrderConfirmationEmail;
use App\Utils\MailerService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class OrderConfirmationHandler implements MessageHandlerInterface
{
    private $mailer;

    private $orderRepository;

    public function __construct(MailerService $mailer, OrderRepository $orderRepository)
    {
        $this->mailer = $mailer;
        $this->orderRepository = $orderRepository;
    }

    public function __invoke(OrderConfirmationEmail $orderId)
    {
        $order = $this->orderRepository->find($orderId->getOrderId());

        /*  dump($order);
        foreach ($order as $key => $value) {
            dump($value);
        }

        die();*/
        // ... do some work - like sending an SMS message!

        $mail = $this->mailer->create(
            'emails/order_confirmation.html.twig',
            'admin@my-ecommerce.fr',
            [
                'order' => $order,
            ]
        );

        sleep(10);

        $this->mailer->send($mail);

        echo 'Sending email now ..';
    }
}
