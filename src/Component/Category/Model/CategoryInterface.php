<?php

declare(strict_types=1);

namespace App\Component\Category\Model;

use App\Component\Product\Model\ProductInterface;
use Doctrine\Common\Collections\Collection;

interface CategoryInterface
{
    public function getId(): ?string;

    public function getName(): ?string;

    public function setName(string $name): void;

    public function getProducts(): Collection;

    public function addProduct(ProductInterface $product): void;

    public function removeProduct(ProductInterface $product): void;
}
