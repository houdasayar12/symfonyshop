<?php

declare(strict_types=1);

namespace App\Component\Core\Model;

trait TimestampableTrait
{
    /**
     *  @ORM\Column(type="datetime", nullable= true)
     *
     *  @var \DateTimeInterface|null
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable= true)
     *
     * @var \DateTimeInterface|null
     */
    private $updatedAt;

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
