<?php

declare(strict_types=1);

namespace App\Component\Product\Factory;

use App\Component\Product\Model\ProductInterface;

interface ProductFactoryInterface
{
    public function createNew(): ProductInterface;

    public function createNamed(): void;
}

/*
    app.product.factory:
        class: App\Component\Product\Factory\ProductFactory
        arguments: [!tagged { tag: 'app.factory' }]


    App\Controller\CartController:
        arguments: ['@app.product.factory']



            _instanceof:
        App\Component\Product\Model\ProductInterface:
            tags: ['app.product']
        App\Component\Product\Factory\Productfactory:
            tags: ['app.product.factory']

    App\Component\Resource\Factory\FactoryInterface: ~

    App\Component\Product\Factory\ProductFactory:
        factory: ['App\Component\Product\Factory\ProductFactory' , 'createNew']
        arguments: ['App\Component\Resource\Factory\FactoryInterface']

        */
