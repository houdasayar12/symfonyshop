<?php

declare(strict_types=1);

namespace App\Component\Product\Model;

use App\Component\Category\Model\Category;
use Doctrine\Common\Collections\Collection;
use phpDocumentor\Reflection\Types\Integer;

interface ProductInterface
{
    /**
     * @return int
     */
    public function getId(): ?string;

    public function setName(string $name): void;

    /**
     * @return string
     */
    public function getName(): ?string;

    /**
     * @param $image
     */
    public function setIllustration(string $image): void;

    /**
     * @return string
     */
    public function getIllustration(): ?string;

    /**
     * @return string
     */
    public function getSubtitle(): ?string;

    /**
     * @return string
     */
    public function getDescription(): ?string;

    /**
     * @param float $description
     */
    public function setDescription(string $description): void;

    /**
     * @param float $subtitle
     */
    public function setSubtitle(string $subtitle): void;

    /**
     * @return int
     */
    public function getStock(): ?Integer;

    /**
     * @param float $stock
     */
    public function setStock(int $stock): void;

    /**
     * @return bool
     */
    public function isBest(): ?bool;

    public function setIsBest(bool $isBest): void;

    public function setPrice(float $price): void;

    /**
     * @return float
     */
    public function getPrice(): ?float;

    public function getOrderItem(): Collection;

    public function setCategory(?Category $category): void;

    public function getCategory(): ?Category;
}
