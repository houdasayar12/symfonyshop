import intlTelInput from 'intl-tel-input';

const input = document.querySelector(".phoneNumber");
intlTelInput(input, {
    initialCountry: 'auto',
    geoIpLookup: function (callback) {
        var elt = document.getElementsByClassName('phoneNumber'),
            current_value = elt.value;
        elt.value = '';  // unset the value before checking geoip
        $.get('https://ipinfo.io', function () { }, "jsonp").always(function (resp) {
            var countryCode = (resp && resp.country) ? resp.country : "";
            callback(countryCode);
            setTimeout(function () {
                elt.value = current_value;  // set value back after geoip is done.
            }, 10);
        });
    },
    utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/js/utils.js"
});