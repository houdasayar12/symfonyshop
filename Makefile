DOCKER_COMPOSE?=docker-compose
RUN=$(DOCKER_COMPOSE) run --rm app
EXEC?=$(DOCKER_COMPOSE) exec -T app
COMPOSER=$(EXEC) composer
CONSOLE=$(EXEC) bin/console
PHPCSFIXER?=$(EXEC) php -d memory_limit=1024m vendor/bin/php-cs-fixer
BEHAT=$(EXEC) vendor/bin/behat
BEHAT_ARGS?=-vvv
PHPUNIT=$(EXEC) vendor/bin/phpunit
PHPUNIT_ARGS?=-v
DOCKER_FILES=$(shell find ./docker/dev/ -type f -name '*')

.DEFAULT_GOAL := help
.PHONY: cc stop

help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

stop:                                                                                                  ## Remove docker containers
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) rm -v --force

.PHONY=create-project
create-project: down start  																		   ## Create the project

.PHONY=start
start:  create-network build up install     ## Start docker container

.PHONY=create-network
create-network: 																					  ## Create network
	docker network inspect app &>/dev/null || docker network create --driver bridge app

.PHONY=build
build: 																								  ## Build docker
	docker-compose build --pull

.PHONY=up
up: 																								   ## Up the containers
	docker-compose up -d

.PHONY=down
down: 																								   ## Kill the containers
	docker-compose down

.PHONY=install
install: start-container install-deps 																   ## Install dependencies and start the project

.PHONY=start-container
start-container: 																					   ## Start the web container
	docker start app

.PHONY=install-deps
install-deps: 																						   ## Install dependencies
	$(EXEC) composer install

.PHONY=code-quality-checker
code-quality-checker: phpqa 																		   ## code quality with PHPQA

cc:                                                                                                    ## Clear the cache in dev env
	$(CONSOLE) cache:clear --no-warmup
	$(CONSOLE) cache:warmup

phpcs: vendor                                                                                           ## Automatically fix PHP Coding Standards
	$(PHPCSFIXER) fix --diff --dry-run --no-interaction -v

phpcsfix: vendor                                                                                       ## Run php-cs-fixer and fix the code.
	$(PHPCSFIXER) fix

security-check: vendor                                                                                 ## Check for vulnerable dependencies
	$(EXEC) vendor/bin/security-checker security:check

phpqa:																						           ## Check code quality with PHPQA
	$(EXEC) ./vendor/bin/phpqa

phpstan: vendor																						   ## Check code quality with PHPQA
	$(EXEC) ./vendor/bin/phpstan analyse src --level=8

.PHONY=phpunit
phpunit: 																							   ## Run phpunit tests
	$(EXEC) ./bin/phpunit

.PHONY=behat
behat:																								   ## Run behat tests
	$(EXEC) ./vendor/bin/behat --colors --strict --stop-on-failure

.PHONY=security-check1
security-check1: vendor/autoload.php ## Check pour les vulnérabilités des dependencies
	$(de) php local-php-security-checker --path=/var/www/app